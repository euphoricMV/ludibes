<?php 

/**
 * Contact page on Ludibes website
 * It will be custom contact form
 */

/**
 * Implements hook_form().
 */
function ludibes_pages_contact_form($form, &$form_state) {
  $form = array();
  $options = array(
    'logo' => t('Logo'),
    'identity' => t('Identity'),
    'website' => t('Website'),
  );
  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#required' => TRUE,
  );
  $form['mail'] = array(
    '#type' => 'textfield',
    '#title' => t('Email'),
    '#required' => TRUE,
  );
  $form['subject'] = array(
    '#type' => 'select',
    '#title' => t('Subject'),
    '#options' => $options,
    '#required' => TRUE,
  );
  $form['message'] = array(
    '#type' => 'textarea',
    '#title' => t('Message'),
    '#required' => TRUE,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );

  return $form;
}